<?php

/**
 * This is for a node of type IMAP
 */
class IMAPService {
  
  var $mbox = NULL;
  var $server;
  var $port;
  var $open = FALSE;
  var $mbox_string = "";
  var $full_con_str = "";
  var $base_con_str = "";
  var $base_url;
  
  function connect($server_addr, $port, $user_name, $pass, $folder = "", $box_options) {

    global $base_url;

    $this->base_url = $base_url;

    $this->server = $server_addr; 
    $this->port = $port;
    if($box_options['server_type'] == 'POP') {
      $options .= '/pop3';
    }
    if($box_options['con_ssl']) {
      $options .= '/ssl';
    }
    if($box_options['no_tls']) {
      $options .= '/notls';
    }
    # Cert
    if($box_options['validate_cert'] == FALSE) {
      $options .= '/novalidate-cert';
    }
    if($box_options['secure']) {
      $options .= '/secure';
    }

    //$name = gethostbyname($service_addr);
    
    $con_str = '{' . $server_addr . ':' .$port . $options . '}' . $folder;
    $this->base_con_str = '{' . $server_addr . ':' .$port . $options . '}';
    $this->full_con_str = $con_str;
    $this->mbox_string = '{' . $server_addr . '}' . $folder;
    imap_timeout(1,1);
    ob_start();
    $try = $this->mbox = imap_open($con_str, $user_name, $pass);
    ob_end_clean();                  
    if($try == FALSE) {
      watchdog('pontomail', 'Could not connect to ' . $service_addr . "|$con_str|" . imap_last_error());
    }
    else {
      $this->open = TRUE;
    }
  }

  function get_mbox_info() {
    return imap_mailboxmsginfo($this->mbox);
  }

  function get_mbox_status() {
    $folders = $this->get_subscriptions();
    foreach($folders as $k => $finfo) {
      $temp = split('}', $finfo->name);
      $folder = $temp[1];
      $foler_obj = NULL;
      $folder_obj = imap_status($this->mbox, $this->base_con_str . $folder, SA_ALL);
      //$folder_quota = imap_get_quotaroot($this->mbox, $finfo->name);
      $folder_obj->quota_storage = $folder_quota['STORAGE'];
      $folder_obj->quota_messages = $folder_quota['MESSAGE'];
      $folder_obj->name = $folder;
      $ans[] =  $folder_obj;
    }

    return $ans; 
  }


  function get_server_name() {
    return $this->server;
  }

  function get_folders($filter = "*") {
    $folders = imap_getmailboxes($this->mbox, '{' . $this->server .':' . $this->port . '}', $filter);
    return $folders;
  }

  function get_all_folders() {
    $folders = imap_getmailboxes($this->mbox, '{' . $this->server .':' . $this->port . '}', "*");
    if(is_array($folders)) {
      foreach($folders as $key => $val) {
        if($val->name) {
          $all_folders[$key]['name'] = $val->name;
          $all_folders[$key]['delimiter'] = $val->delimiter;
          $all_folders[$key]['attributes'] = $val->attributes;
        }
      }
    }
    else {
      watchdog('IMAPService', t('Unable to retrive all mail boxes'), WATCHDOG_WARNING);
    }
    return $all_folders;
  }

  function get_subscriptions($filter = "*") {
    $folders = imap_getsubscribed($this->mbox, '{' . $this->server .':' . $this->port . '}', $filter);
    return $folders;
  }

  function folder_subscribe($folder_name = "", $subscribe = FALSE) {
    if($folder_name == "") {
      return;
    }
    if($subscribe) {
      $try = imap_subscribe($this->mbox, $folder_name);
    }
    else {
      $try = imap_unsubscribe($this->mbox, $folder_name);
    }
    return $try;
  }

  function folder_rename($folder, $new_folder_name) {
    if(trim($new_folder_name) == "") {
      return;
    }
    $try = imap_renamemailbox($this->mbox, $folder, $new_folder_name);
  }

  function folder_new($folder) {
    if(trim($folder) == "" ) {
      return false;
    }
    $folder = imap_utf7_encode($this->base_con_str.$folder);
    $try = imap_createmailbox($this->mbox, $folder);
    return $try;
  }
  
  /*
   * Save a raw email to a folder
   *
   */
  function msg_save($raw_msg, $folder) {
    $try = imap_append($this->mbox, $folder, $raw_msg);
    return $try;
  }

  function folder_delete($folder) {
    if(trim($folder) == "" ) {
      return false;
    }
    return imap_deletemailbox($this->mbox, $folder);
  }


  function get_headers($max_pages = 50) {
    global $user;
    $sorted_msgs=imap_sort($this->mbox,SORTARRIVAL,1); 
    # Respect Paging from Drupal
    if($_GET['page'] != "") {
      $max = ceil($sorted_msgs[0] - (($_GET['page'] + 1) * $max_pages));
      $max = $max < 0 ? $max + $max_pages : $max;
    }
    else {
      $max = $sorted_msgs[0];
    }
    $min = $max > $max_pages ? $max - $max_pages - 1 : $sorted_msgs[(count($sorted_msgs) - 1)];
    $headers = imap_fetch_overview($this->mbox, $max.':'.$min);
    if(!$headers) {
      $erros = imap_errors();
      // TODO Place this error in the watch dog.
      return array();
    }
    $headers = array_reverse($headers);
    db_query("DELETE FROM {pontomail_pager_holder} WHERE uid = %d AND id = %d", $user->uid, $this->server);
    for($i = 0; $i < $sorted_msgs[0]; $i++) {
      db_query("INSERT INTO {pontomail_pager_holder} (uid, id) VALUES(%d, '%s')", $user->uid, $this->server);
    }
    return $headers;
  }

  function num_new_msgs($folder = "") {
    $msg_count = "";
    if($folder) {
      $con_string = '{' . $this->server . '}' . $folder;
      $obj = imap_status($this->mbox, $con_string, SA_ALL);
    }
    else {
      $msg_count = imap_num_recent($this->mbox);
    }
    return $msg_count;
  }

  function folder_status($folder) {
    $con_string = '{' . gethostbyname($this->server) . ':' . $this->port . '}' . $folder;
    $obj = imap_status($this->mbox, $con_string, SA_ALL);
    return $obj;
  }

  function close() {
    if($this->open) {
      imap_close($this->mbox);
    }
    $this->open = FALSE;
    return;
  }

  function open() {
    return $this->open;
  }

  function get_raw_message($mid) {
    return htmlspecialchars(imap_fetchheader($this->mbox, $mid, SE_UID).imap_body($this->mbox, $mid, SE_UID));
  }

  function get_message($mid) {
    $obj = imap_fetchstructure($this->mbox, $mid, SE_UID); 
    $attachments = array();
    $this->load_attachments($obj, $attachments);
    $obj->attachments = $attachments;
    
    # Add the body to it.
    $obj->headers = imap_headerinfo($this->mbox, imap_msgno($this->mbox, $mid));
    
    # Clean the body
    # If PLAIN ,just set body to body
    if(strtoupper($obj->subtype) == 'PLAIN') {
      $obj->body = imap_body($this->mbox, $mid, SE_UID); 
      $obj->display_types['PLAIN']['data'] = imap_body($this->mbox, $mid, SE_UID); 
    }
    if(strtoupper($obj->subtype) == 'HTML') {
      $obj->body = $this->mimeDecode($this->get_body_part($mid, "1"), $obj->encoding);
      $obj->display_types['HTML']['data'] = $this->mimeDecode($this->get_body_part($mid, "1"), $obj->encoding);
      $obj->display_types['HTML']['data'] = str_replace('<body', '<body style="nothing.css"', $obj->display_types['HTML']['data']);
    }
    else {
      # Loop throught the parts
      $alternatives = $this->locate_alternatives($obj, 0, $mid);
      if(strtoupper($obj->subtype) == 'RELATED') {
        $alternatives['HTML']['data'] = $this->parseRelatedHTML($alternatives['HTML']['data']);
      }
      $obj->display_types = $alternatives;
    }
    
    # End Cleaning
    $obj->uid = $uid;
    $obj->mid = $mid;

    return $obj;
  }

  function load_attachments($parts, &$acc) {
    # Loop though and set attachements
    if(!$parts->parts) {
      return;
    }
    $base_url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
    $replace_url =  url('/pontomail/' . arg(1) . '/' . arg(2) 
      . '/' . arg(3) . '/' . arg(4) . '/' . arg(5));
    foreach($parts->parts as $key => $part) {
      if(strtoupper($part->disposition) == "ATTACHMENT" || $this->mime_type_check($part->type, $part->subtype)) {
        $temp = array();
        $mime_info = $this->get_mime_type($part->subtype);
        $temp['mime'] = $mime_info['mime'];
        $temp['mime_img'] = $mime_info['img'];
        $temp['encoding'] = $part->encoding;
        foreach($part->parameters as $k => $param) {
          if(strtoupper($param->attribute) == 'NAME') {
            $temp['name'] = $param->value;
          }
        }
        $temp['bytes'] = $part->bytes;
        $temp['size'] = $part->bytes < 1024 ? $part->bytes : ($part->bytes/1024);
        $temp['size_suffix'] =  $part->bytes < 1024 ? 'Bytes' : 'KB';
        $temp['size'] = round($temp['size'], 2);
        $temp['type'] = $part->type;
        $temp['pid'] =  $key + 1;
        $temp['link'] = $this->base_url . $replace_url . '&attachment=' . $temp['pid'];
        $width = '100px';
        $height = '100px';
        // TODO may need to revisit
        //echo "<br/>Part: <pre>" . print_r($part, 1) . "</pre><br/><pre>" . print_r($mime_info, true) . "</pre>"; die();
        if($mime_info['img'] != "") {
          $temp['html_preview'] = "<img src='" . $this->base_url . '/' . $mime_info['img'] . "' 
            style='border:10; border-color: red' alt='" . $temp['name'] . "'/>";
        }
        else {
          $temp['html_preview'] = "<img src='" . $this->base_url . $replace_url . "&inline=" . $temp['pid'] . "' width='" . $width . "' height='" . $height . "'  
          style='border:10; border-color: red' alt='" . $temp['name'] . "'  />";
        }
        $acc[] = $temp;
      }
    }
    return;
  } // end function load_attachementS

  function mime_type_check($type, $subtype) {

    if($type == TYPEAPPLICATION || $type == TYPEAUDIO || $type == TYPEIMAGE ||
       $type == TYPEVIDEO || $type == TYPEOTHER) {
      return true;
    } else {
      return false;
    }
  }


  function get_attachment($mid, $pid, $disposition = "attachment") {
    $obj = imap_fetchstructure($this->mbox, $mid, SE_UID);
    $found = FALSE;
    foreach($obj->parts as $k => $part) {
      if(($k + 1) == $pid) {
        $found = TRUE;
        break;
      }
    }

    if(!$found) {
      watchdog('pontomail_sendmail', 'Invalid Part Number', WATCHDOG_WARNING);
      return;
    }

    foreach($part->parameters as $k => $param) {
      if(strtoupper($param->attribute) == 'NAME') {
        $filename = $param->value;
      }
    }
    if($filename == "") {
      $filename = rand();
    }

    $mime = $this->getMIME($part->type, $part->subtype);
    $body_part = $this->get_body_part($mid, $pid);
    $data = $this->mimeDecode($body_part, $part->encoding);
    if($disposition == 'object') {
      $part->filemime = $mime;
      $part->filename = $filename;
      $part->imap_encoding = $part->encoding;
      $part->encoding = $this->encoding_int_to_str($part->encoding);
      //$part->decoded_data = $data;
      $part->data = $body_part;
      return $part;
    }
    
    ob_clean();

    header('Pragma: public');
    header('Expires: 0');
    header('Content-Type: ' . $mime);
    #header('Content-Length: ' . $size); // How are we going to get the size?
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Cache-Control: private',false);
    header('Content-Disposition: ' . $disposition . '; filename="' . $filename . '"');
    echo $data;
    ob_flush();
    exit;
    
    return;
  }
  
  function get_by_cid($parts, $mid, $cid) {
    # Look Through and Find it.
    $obj = imap_fetchstructure($this->mbox, $mid, SE_UID);
    foreach($obj->parts as $k => $look) {
      if($look->id == "<$cid>") {
        $part = $look;
        $pid = (string)($k+1); 
      }
    }
    
    if($pid == "") {
      return false;
    }
    $mime = $this->getMIME($part->type, $part->subtype);
    $data = $this->mimeDecode($this->get_body_part($mid, $pid), $part->encoding);
    $test = $this->get_body_part($mid, $pid);

    # New Headers
    ob_clean();
    
    $send_data = base64_decode(stripslashes(str_replace(' ', '', trim($test))));
    $size = strlen($send_data);
    $filename = $cid;
    
    header("Pragma: public");
    header("Expires: 0");
    header('Content-Type: ' . $mime);
    header('Content-Length: ' . $size);
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false);
    header("Content-Disposition: inline");

    # Keep around. May need to use this for certain MIME types.
    /*header("Content-Type: application/octet-stream");
    header("Content-Disposition: attachment; filename=\"".$filename."\";");
    header("Content-Transfer-Encoding: binary");
    */
    echo $send_data;
    ob_flush();
    exit;
    return;
    
  }
  

  function getMIME($type, $subtype) {
    $mime = "";
    switch($type) {
      case TYPEIMAGE:
        $type = 'image';
        break;
      default:
        $type = 'text';
        break;
    }

    $mime = $type . '/' . $subtype;
    return $mime;
  }

  function locate_alternatives($parts, $cur = "", $mid){
    if(strtoupper($parts->subtype) == 'ALTERNATIVE') {
      $temp = array();
      foreach($parts->parts as $k => $leaf) {
        if($cur == 0) {
          $cp = $k + 1;
        } else {
          $cp = $cur . '.' . (string)($k+1);
        }
        $temp[$leaf->subtype]['data'] = $this->mimeDecode($this->get_body_part($mid, $cp), $leaf->encoding);
        $sub_v = strrev($cp);
      } 
      return $temp;
    }
    elseif(strtoupper($parts->subtype) == 'PLAIN' || strtoupper($parts->subtype) == 'HTML') {
      $temp = array();
      $temp[$parts->subtype]['data'] = $this->mimeDecode($this->get_body_part($mid, $cur), $parts->encoding);
      return $temp;
    }
    elseif(is_array($parts)) {
      foreach($parts as $k => $part) {
        if($cur > 1) {
          $cur = (string)$cur . '.' . (string)($k+1);
        } else {
          $cur = 1;
        }
        return $this->locate_alternatives($part, $cur, $mid);
      }
    }
    else {
      if(is_array($parts->parts)) {
        if($cur == "") {
          $cur = "1";
        }
        else {
          $cur++;
        }
        return $this->locate_alternatives($parts->parts, $cur, $mid);
      }
    }// End Else
  }// end locate_alternatives function

  function mimeDecode($data, $encoding) {
    switch($encoding) {
      case ENC8BIT:
        $data = imap_8bit($data);
        $data = quoted_printable_decode($data);
        break;
      case ENCBASE64:
        $data = base64_decode(stripslashes($data));
        break;
      case ENCQUOTEDPRINTABLE:
        $data = quoted_printable_decode($data);
        break;
    }
    //echo "Encoding: " . $encoding . " | " . ENCBASE64;
    return $data;
  }

  function encoding_int_to_str($encoding) {
    switch($encoding) {
      case ENC8BIT:
        $ec = "8bit";
        break;
      case ENCBASE64:
        $ec = "base64";
        break;
      case ENCQUOTEDPRINTABLE:
        $ec = "quoted-printable";
        break;
    }
    return $ec;
  }

  function get_mime_type($type = "") {
    $img = "";
    $path =  drupal_get_path('module', 'pontomail') . '/mimeicons';
    switch(strtoupper($type)) {
      # Applications
      case 'PDF':
      case 'PS':
      case 'POST-SCRIPT':
        $mime = 'application/pdf';
        $img = "$path/page_white_acrobat.png";
        break;
      case 'MSWORD':
        $mime = 'application/msword';
        $img = "$path/page_word.png";
        break;
      case 'PNG':
      case 'GIF':
      case 'BMP':
      case 'JPG':
      case 'JPEG':
        $mime = "image/$type";
        break;
      case 'MP3':
      case 'WMA':
      case 'X-MS-WMA':
      case 'AAC':
        $mime = "audio/$type";
        $img = "$path/sound.png";
        break;
      case 'MPEG':
      case 'WMV':
      case 'AVI':
        $mime = "video/$type";
        $img = "$path/film.png";
        break;
      default:
        $mime = 'text/x-generic';
        $img = "$path/application.png";
    }
    if(variable_get('pontomail_log_unknown_mime_types', false)) {
      watchdog('pontomail', "Unknown mime-type logged: <em>$type<em> is not recognized.");
    }
    return array('mime' => $mime, 'img' => $img);
  }

  function parseRelatedHTML($data) {
    # We want to replace with part getters.
    $base_url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
    $replace_url =  url('/pontomail/' . arg(1) . '/' . arg(2) 
      . '/' . arg(3) . '/' . arg(4) . '/' . arg(5) . '&cid=');

    $html = str_replace('"cid:', '"' . $base_url . $replace_url, $data);
    
    return $html;
  }

  function get_mime_img($app) {
    $base_url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '/modules/pontomail/';
    return $url;
    
  }
    



  function get_body_part($mid, $pid) {
    if($pid == "")
      return;
    $item = imap_fetchbody($this->mbox, $mid, $pid, FT_UID); 
    return $item;
  }
  
  function mark_read($mid) {
    $this->get_body_part($mid, 1);
    return TRUE;
  }

  function mark_delete($mid) {
    imap_delete($this->mbox, $mid, FT_UID);
    return;
  }

  function mark_undelete($mid) {
    imap_undelete($this->mbox, $mid, FT_UID);
    return;
  }

  function expunge() {
    imap_expunge($this->mbox);
    return;
  }

  function get_folder_name($folder_name){
    $folder_name = str_replace('{' . $this->server .':' . $this->port . '}',
      '',
      imap_utf7_decode($folder_name));
    return $folder_name;
  }

  function get_errors() {
    return imap_errors();
  }

  function get_last_error() {
    return imap_last_error();
  }

}

?>
