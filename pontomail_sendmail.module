<?php

/**
 * @file
 * Enables the use of personal contact forms.
 */

/**
 * Implementation of hook_help().
 */
function pontomail_sendmail_help($section) {
  switch ($section) {
    case 'admin/help#pontomail_sendmail':
      $output = '<p>'. t('The contact module allows other users to contact you by e-mail via your personal contact form.   Users can send a subject and message in the contact form. The contact module is important in helping make connections among members of your community.') .'</p>';
      $output .= '<p>'. t('Users can administer the contact settings in their account settings.  Note that a users e-mail address is not made public and that privileged users such as site administrators are able to contact you even if you choose not to enable this feature. If users activate the personal contact form, then a contact tab will appear in their user profile.') .'</p>';
      $output .= t('<p>You can</p>
<ul>
<li>view user <a href="%profile">profiles</a>.</li>
<li>enable the personal contact form in <a href="%admin-user">administer &gt;&gt; user &gt;&gt; edit tab &gt;&gt; account settings tab &gt;&gt; personal contact settings</a>.</li>
</ul>
', array('%profile' => url('profile'), '%admin-user' => url('admin/user')));
      $output .= '<p>'. t('For more information please read the configuration and customization handbook <a href="%contact">Contact page</a>.', array('%contact' => 'http://www.drupal.org/handbook/modules/contact/')) .'</p>';
      return $output;
    case 'admin/modules#description':
      return t('Enables on to email users using attachments.');
    case 'admin/pontomail_sendmail':
      return t('This page lets you setup <a href="%form">your site-wide contact form</a>.  To do so, add one or more subjects.  You can associate different recipients with each subject to route e-mails to different people.  For example, you can route website feedback to the webmaster and direct product information requests to the sales department.  On the <a href="%settings">settings page</a> you can customize the information shown above the contact form.  This can be useful to provide additional contact information such as your postal address and telephone number.', array('%settings' => url('admin/settings/pontomail_sendmail'), '%form' => url('pontomail_sendmail', NULL, NULL, TRUE)));
    case 'node/add#pontomail_sendmail':
      return t('Compose and send an email. You are also to emails with attachments and/or send an email using HTML formatting.');
  }
}

/**
 * Implementation of hook_menu().
 */
function pontomail_sendmail_menu($may_cache) {
  global $user;
  $items = array();

  $user_access = user_access('access content');
  $access_send_email = user_access('send email');
  $access_site_config = user_access('administer pontomail');

  if ($may_cache) {
    $items[] = array('path' => 'pontomail_sendmail', 'title' => t('contact us'),
      'callback' => 'pontomail_sendmail_mail_page', 'access' => $access_send_email,
      'type' => MENU_SUGGESTED_ITEM);
    $items [] = array('path' => 'node/add/pontomail_sendmail', 'title' => t('compose email'),
      'access' => $access_send_email);
    $items[] = array('path' => 'pontomail/compose', 'title' => t('compose'),
      'callback' => 'pontomail_sendmail_compose', 'access' => $access_send_email);
    $items[] = array('path' => 'pontomail_sendmail/autocomplete', 'title' => t('pontomail_sendmail autocomplete'),
      'callback' => 'pontomail_sendmail_autocomplete', 'access' => $access_send_email,
      'type' => MENU_CALLBACK);
  }
  else {
    
    /*if (arg(0) == 'user' && is_numeric(arg(1))) {
      $items[] = array('path' => "user/". arg(1) ."/pontomail", 'title' => t('contact'),
        'callback' => 'pontomail_sendmail_mail_user', 'type' => MENU_LOCAL_TASK, 'weight' => 2);
    }*/
    if((arg(0) == 'pontomail' && arg(1) == 'viewmailbox' && is_numeric(arg(5))) && $access_send_email) {
      $temp = explode('/', $_GET['q']);
      for($i=0;$i<6;$i++) {
        if($cur_path == "") {
          $cur_path = $temp[$i];
        } else {
          $cur_path .= '/' . $temp[$i];
        }
      }

      // Call back items
      $items[] = array('path' => $cur_path . '/reply', 'title' => t('reply'),
        'callback' => '_pontomail_sendmail_reply', 'callback arguments' => array((Object)$node), 
        'type' => MENU_LOCAL_TASK);
      $items[] = array('path' => $cur_path . '/forward', 'title' => t('forward'),
        'callback' => '_pontomail_sendmail_reply',
        'type' => MENU_LOCAL_TASK,
        'weight' => 2,
        'callback arguments' => array((Object)$node, 'forward'));
    }
  }

  return $items;
}

function pontomail_sendmail_compose() {
  drupal_goto('node/add/pontomail_sendmail');
  return;
}


function pontomail_sendmail_node_info() {
  return array('pontomail_sendmail' =>
    array('name' => t('Email'), 'base' => 'pontomail_sendmail'));
}

function pontomail_sendmail_perm() {
  return array('send email');
}


/**
 * Implementation of hook_settings().
 */
function pontomail_sendmail_settings() {
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE);
  $form['ldap'] = array(
    '#type' => 'fieldset',
    '#title' => t('LDAP Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE);
  $form['general']['pontomail_sendmail_form_information'] = array(
    '#type' => 'textarea', '#title' => t('Global Email Footer'),
    '#default_value' => variable_get('pontomail_sendmail_global_footer', ''),
    '#description' => t('This will be placed on every email that is send using Drupal. Leave blank for no footer.')
  );
  $form['general']['pontomail_sendmail_hourly_threshold'] = array(
    '#type' => 'select',
    '#title' => t('Hourly threshold'),
    '#options' => drupal_map_assoc(array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40, 50)),
    '#default_value' => variable_get('pontomail_sendmail_hourly_threshold', 3),
    '#description' => t('The maximum number of e-mail submissions a user can perform per hour.'),
  );
  $form['general']['pontomail_sendmail_max_attachments_size'] = array(
    '#type' => 'select',
    '#title' => t('Maximum Attachments Size'),
    '#options' => drupal_map_assoc(array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40, 50, 100)),
    '#default_value' => variable_get('pontomail_sendmail_max_attachments_size', 2),
    '#description' => t('The maximum size that the sum of all the attachments in an email to be sent can be.'),
  ); 
  # LDap Settings
  $form['ldap']['pontomail_sendmail_ldap_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable LDAP Searching'),
    '#default_value' => variable_get('pontomail_sendmail_ldap_enable', FALSE));
  $form['ldap']['pontomail_sendmail_ldap_server'] = array(
    '#type' => 'textfield',
    '#title' => t('LDAP Server'),
    '#default_value' => variable_get('pontomail_sendmail_ldap_server', 'localhost'),
    '#description' => t('The location of your LDAP server.')
    ); 
  $form['ldap']['pontomail_sendmail_ldap_type'] = array(
    '#type' => 'select',
    '#title' => t('LDAP Server Type'),
    '#default_value' => variable_get('pontomail_sendmail_ldap_type', 'General'),
    '#options' => array('General' => t('General'), 'ADS' => t('Active Directory')),
    '#description' => t('If you are using an ADS Server (Active Directory Services) for your LDAP, you need to specify that here.
    Else just use <em>General</em>')
    ); 
  $form['ldap']['pontomail_sendmail_ldap_port'] = array(
    '#type' => 'textfield',
    '#title' => t('LDAP Port'),
    '#default_value' => variable_get('pontomail_sendmail_ldap_port', '389'),
    '#size' => '4'
    );
  $form['ldap']['pontomail_sendmail_ldap_user'] = array(
    '#type' => 'textfield',
    '#title' => t('User Name'),
    '#default_value' => variable_get('pontomail_sendmail_ldap_user', ''),
    '#description' => t('For some LDAP servers and usually for 
    Active Directory, your user name may need to include your 
    domain name: e.g. <em>user@example.com</em>')
    );
  $form['ldap']['pontomail_sendmail_ldap_pass'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => variable_get('pontomail_sendmail_ldap_pass', '')
    );
  $form['ldap']['pontomail_sendmail_ldap_dn'] = array(
    '#type' => 'textfield',
    '#title' => t('LDAP DN'),
    '#default_value' => variable_get('pontomail_sendmail_ldap_dn', ''),
    '#description' => t('The DN that you wish to use. For example, if your LDAP server is at <em>example.com</em>, try using the dn <em>DC=example,DC=com</em>')
    );
  $form['ldap']['pontomail_sendmail_ldap_filter'] = array(
    '#type' => 'textfield',
    '#title' => t('Additional LDAP Filters'),
    '#default_value' => variable_get('pontomail_sendmail_ldap_filter', ''),
    '#description' => t('Enter any additional filters you would like to include in the LDAP query. For example, if you wanted only include results
    in the Drupal group or the name Drupal, you may say <em> (| (objectClass=Drupal) (CN=Drupal) )</em>. This is nice to use if you want
    to only display certain groups/people. In general though, you can leave this blank. Please be sure to include parenthesis in your 
    filter: e.g. <em> ( [filter] ) </em>.')
    );
  
  return $form;
}

/**
 * Implementation of hook_user().
 *
 * Provides signature customization for the user's comments.
 */
function pontomail_sendmail_user($type, $edit, &$user, $category = NULL) {
  /*
  if ($type == 'form' && $category == 'account') {
    $form['contact'] = array('#type' => 'fieldset', '#title' => t('Contact settings'), '#weight' => 5, '#collapsible' => TRUE);
    $form['contact']['contact'] = array('#type' => 'checkbox', '#title' => t('Personal contact form'), '#default_value' => $edit['contact'], '#description' => t('Allow other users to contact you by e-mail via <a href="%url">your personal contact form</a>. Note that your e-mail address is not made public and that privileged users such as site administrators are able to contact you even if you choose not to enable this feature.', array('%url' => url("user/$user->uid/contact"))));
    return $form;
  }
  if ($type == 'validate') {
    return array('contact' => $edit['contact']);
  }
  */
  return;
}


function _pontomail_sendmail_reply() {
  global $user;
  # Load the imap message
  $op = $_POST['op'];
  $edit = $_POST['edit'];

  $fwd = arg(6) == 'forward';

  if($op == t('Send e-mail')) {
    pontomail_sendmail_node_form_submit('pontomail_sendmail', $edit);
    return;
  }
  
  $message_obj = NULL;
  $sid = arg(3); $folder = arg(4); $mid = arg(5);
  if(trim($sid) != "" ) {
    $account_info = db_fetch_object(db_query('SELECT * FROM {pontomail_servers} WHERE sid = %d', $sid));
    $imap = _pontomail_get_imap_obj($sid, $folder);
    $message_obj = $imap->get_message($mid);
  }
  
  $form = _pontomail_sendmail_form_composition($sid, $message_obj);
  // Setup to attach upload.module
  $form['type'] = array('#type' => 'hidden', '#value' => 'pontomail_sendmail');
  $form['#node'] = (object)array(); // Give it a dummy object
  if(function_exists('upload_form_alter')) {
    upload_form_alter('pontomail_sendmail_node_form', $form);
  }

  return drupal_get_form('pontomail_sendmail_form', $form);
}

function pontomail_sendmail_form(&$node) {
  global $user;
  $edit = $_POST['edit'];
  $node->date = format_date(time(), 'custom', 'Y-m-d H:i:s O');

  if ($account = user_load(array('uid' => arg(1), 'status' => 1)) || arg(2) == 'pontomail_sendmail' || arg(0) == 'pontomail') {
    if (!$account->contact && !user_access('administer users')) {
      $form[] = t('%name is not accepting e-mails.', array('%name' => $account->name));
    }
    else if (!$user->uid) {
      $form[] = t('Please <a href="%login">login</a> or <a href="%register">register</a> to send %name a message.', array('%login' => url('user/login'), '%register' => url('user/register'), '%name' => $account->name));
    }
    else if (!valid_email_address($user->mail)) {
      $form[] = t('You need to provide a valid e-mail address to contact other users. Please edit your <a href="%url">user information</a>.', array('%url' => url("user/$user->uid/edit")));
    }
    else if (!flood_is_allowed('contact', variable_get('pontomail_sendmail_hourly_threshold', 3))) {
      $form[] = t("You can't contact more than %number users per hour. Please try again later.", array('%number' => variable_get('pontomail_sendmail_hourly_threshold', 3)));
    }
    else {
        $form = _pontomail_sendmail_form_composition();
    }
    return $form;
  }
}

function _pontomail_sendmail_form_composition($sid = "", $message_obj = NULL) {
  global $user;
  $fwd = arg(6) == 'forward';
      drupal_set_title($account->name);
      $user_email =  $user->name .' &lt;'. $user->mail .'&gt;';
      if($sid != "") {
        if($fwd) {
          $subject = "FW: " . $message_obj->headers->subject;
        }
        else {
          $subject = "Re: " . $message_obj->headers->subject;
        }
        if(!$fwd) {
          $to = $message_obj->headers->toaddress;
        }
        $from = $message_obj->headers->fromaddress;
        $message = $message_obj->display_types['PLAIN']['data'];
        if(!trim($message)) {
          $message = $message_obj->display_types['HTML']['data'];
        }
        if($fwd) {
          $message = "\n\n\nForwarded Message\n----------------------------------\n$message";
        }
        else {
          $temp = explode("\n", $message);
          $message = '>' . implode(">\n", $temp);
          $message = "\n\n\nRE: \n" . $message;
        }
        $temp = explode('/', $_GET['q']);
        for($i=0;$i<6;$i++) {
          if($msg_path == "") {
            $msg_path = $temp[$i];
          } else {
            $msg_path .= '/' . $temp[$i];
          }
        }
        $msg_path . '/view';
      } 

      $form['#token'] = $user->name . $user->mail;
      // Attachments Section
      $attachments = $message_obj->attachments;
      if($attachments && $fwd) {
        $form['mid'] = array('#type' => 'hidden', '#value' => $message_obj->mid);
        $form['folder'] = array('#type' => 'hidden', '#value' => arg(4));
        $form['fwd_attachment_group'] = array('#type' => 'fieldset',
                                            '#title' => t('Attachments'),
                                            '#collapsible' => TRUE,
                                            '#collapsed' => FALSE,
                                            '#description' => t('Select attachments you would like to forward
                                            with this email'));
        $form['fwd_attachment_group']['attachments']['#tree'] = TRUE;
        $form['fwd_attachment_group']['attachments']['#theme'] = 'pontomail_attachments_table';

        foreach($attachments as $k => $a) {
          /*
          $info = "<strong>Name:</strong> " . $a['name'] . "<br/>";
          $info .= "<strong>Size:</strong> " . $a['size'] . " " . $a['size_suffix'] . "<br/>";
          $info .= "<strong><a href='" . $a['link'] . "'>Download</a></strong><br/>";
          if($a['html_preview']) {
            $info .= $a['html_preview'] . "<br/>";
          }
          $info .= "<br/>";
          */
          $link = "<strong><a href='" . $a['link'] . "'>Download</a></strong>";
          //$form['fwd_attachment_group']['attachments'][$a['name']] = array('#value' => $info);
          $form['fwd_attachment_group']['attachments'][$a['pid']]['fwd'] = array('#type' => 'checkbox');
          $form['fwd_attachment_group']['attachments'][$a['pid']]['preview'] = array('#value' => $a['html_preview']);
          $form['fwd_attachment_group']['attachments'][$a['pid']]['name'] = array('#value' => $a['name']);
          $form['fwd_attachment_group']['attachments'][$a['pid']]['size'] = array('#value' => $a['size']);
          $form['fwd_attachment_group']['attachments'][$a['pid']]['download'] = array('#value' => $link);
          
        }
        //$page .= drupal_get_form('attachments', $form);
      }

        // End Attachements

      // Check to see if we have a server listing
      $sid = arg(3);
      if(!is_numeric($sid)) {
        $rs = db_query("SELECT sid, server_name FROM {pontomail_servers} WHERE uid = %d", $user->uid);
        while($row = db_fetch_object($rs)) {
          $servers[$row->sid] = $row->server_name; 
        }
        $form['sid'] = array('#type' => 'select',
          '#title' => t('Email account to use'),
          '#options' => $servers);
      }
      else {
        $form['sid'] = array('#type' => 'hidden', '#value' => $sid);
      }
      $form['from'] = array('#type' => 'item', '#title' => t('From'), '#value' => $user_email);
      $form['to'] = array('#type' => 'textfield', 
        '#title' => t('To'), 
        '#default_value' => $from, 
        //'#required' => TRUE,
        '#autocomplete_path' => 'pontomail_sendmail/autocomplete',
        '#description' => t('Seperate multiple email addresses with a comma (,). Also,
        please not that entires that were found via LDAP and are not local to Drupal will be show with
        an <em>*</em> in front of them. This is only availiable if you have javascript enabled.'));
      $form['subject'] = array('#type' => 'textfield', '#title' => t('Subject'), '#maxlength' => 50, '#required' => TRUE, '#default_value' => $subject);
      $form['message'] = array('#type' => 'textarea', 
        '#title' => t('Message'), 
        '#rows' => 15, '#required' => TRUE, 
        '#default_value' => $message);
      $form['send_format'] = array('#type' => 'select', 
        '#title' => t('Send As'), 
        '#options' => array('html' => 'HTML', 'plain' => 'Plain'),
        '#description' => t('The format that you would like to send the message in.<br/>Unless you are using HTML formatting or using the Tiny MCE
        editor to create your emails, it is recommended that you use <em>Plain</em> as your sending format; otherwise use <em>HTML</em>.')
        );
      
      $form['copy'] = array('#type' => 'checkbox', '#title' => ('Send me a copy.'));
      // End setup.
      $form['send_email'] =  array('#type' => 'submit', '#value' => t('Send e-mail'));
      if($msg_path != "") {
        $form['destination'] = array('#type' => 'hidden', '#value' => $msg_path);
      }
      return $form;
}

function pontomail_sendmail_form_alter($form_id, &$form) {
  // Take away: author, options, user_comments, menu
  if($form_id == 'pontomail_sendmail_node_form') {
    $filter_form_elements = array('nid','vid','uid','created','changed','type',
      'from','to','subject','message','send_format','copy','send_email','title',
      'form_token','form_id', 'sid', 'create_node');
    foreach($form as $name => $structure) {
      if($name[0] != '#' && !in_array($name, $filter_form_elements)) {
        $form[$name] = array('#type' => 'value', '#value' => 'value');
      }
    }
    $form['date'] = array('#type' => 'hidden', '#value' => format_date(time(), 'custom', 'Y-m-d H:i:s O'));
  }
  return;
}

function pontomail_sendmail_node_form_submit($form_id, $edit) {
  global $user;

  $mime_wrap = 90;
  $crlf = "\r\n";
  $nl = "\n";

  $send_format = strtoupper($edit['send_format']);
  $imap_obj = _pontomail_get_imap_obj($edit['sid'], $edit['folder']);
  // Get any attachments
  // TODO Look at using a drupal function instead of
  // directly at the SESSION var
  $attachments = $_SESSION['file_previews'];
  $boundary = $user->timestamp . $user->name . $user->login;
  if($attachments) {
    # Loop through and prep each
    foreach($attachments as $key => $file) {
      $mime = split('/', $file->filemime);
      if($mime[0] != 'text' || $mime[0] != 'html') {
        $file->data = chunk_split(base64_encode(file_get_contents($file->filepath)));
        $file->encoding = 'base64';
      } else {
        $file->data = wordwrap(file_get_contents($file->filepath), $mime_wrap);
        $file->encoding = 'text';
      }
      $attachment_msg .= "--$boundary\n";
      $attachment_msg .= 'Content-Type: ' . $file->filemime . '; name= "' . $file->filename . '"' . $nl;
      $attachment_msg .= 'Content-Disposition: attachment; filename="' . $file->filename . '"' . $nl;
      $attachment_msg .= 'Content-Transfer-Encoding: ' . $file->encoding . $nl;
      $attachment_msg .= $crlf;
      $attachment_msg .= $file->data . $nl . $crlf;
    }

    # Add this information to the message.
  }
  unset($_SESSION['file_previews']);
  // Now check and see if we have any forwarding attachements
  if(is_array($edit['attachments'])) {
    foreach($edit['attachments'] as $pid => $checked) {
      $file = $imap_obj->get_attachment($edit['mid'], $pid, "object");
      // Its all already encoded in a raw format
      $attachment_msg .= "--$boundary\n";
      $attachment_msg .= 'Content-Type: ' . $file->filemime . '; name= "' . $file->filename . '"' . $nl;
      $attachment_msg .= 'Content-Disposition: attachment; filename="' . $file->filename . '"' . $nl;
      $attachment_msg .= 'Content-Transfer-Encoding: ' . $file->encoding . $nl;
      $attachment_msg .= $crlf;
      $attachment_msg .= $file->data . $nl . $crlf;
    }
  }
  # Add this information to the messeage.

  $account = user_load(array('uid' => arg(1), 'status' => 1));
  // Compose the body:
  if($send_format == 'HTML') {
    $edit['message'] = check_markup($edit['message']);
  }
  // TODO Use html2txt here for PLAIN format
  $message[] = $edit['message'];

  # Note here for future access control to append all emails with disclaimer.
  # Gaurd agaisnt users using drupal to spam.
  //$message[] = t('Please report offensive, abusive, or unsolicited emails to: '); 

  // Tidy up the body:
  foreach ($message as $key => $value) {
    $message[$key] = wordwrap($value);
  }

  // Prepare all fields:
  $from = $user->mail;
  $to = $edit['to'];
  // Format the subject:
  // TODO Make this a admin variable
  // so that a site admin can choose that any email sent
  // through pontomail is tagged with his site to help fight
  // anyone who may try and use their durpal site for spam.
  //$subject = '['. variable_get('site_name', 'drupal') .'] '. $edit['subject'];
  $subject = $edit['subject'];

  // Prepare the body:
  
  $body = implode("\n", $message);
  if($attachment_msg) {
    if($send_format == 'HTML') {
      $body = "This is a multipart mime message.\n$crlf--$boundary\nContent-Type: text/html; charset=UTF-8;\n$crlf" . $body . "\n" . $crlf;
    } else {
      $body = "This is a multipart mime message.\n$crlf--$boundary\nContent-Type: text/plain; charset=UTF-8;\n$crlf" . $body . "\n" . $crlf;
    }
    $body .= $attachment_msg;
    $body .= "--$boundary--$crlf";
    $body .= $crlf;
  } 

  // Send the e-mail:
  
  $msg_date_stamp = date('r');
  $header = "Date: $msg_date_stamp\nFrom: $from\nReply-to: $from\nX-Mailer: Pontomail\nReturn-path: $from\nErrors-to: $from";
  $header .= "\nUser-Agent: Pontomail Webmail Client [Drupal]";
  if(!$attachment_msg && $send_format != 'PLAIN') {
    $header .= $crlf . "MIME-Version: 1.0";
    $header .= $crlf . "Content-Transfer-Encoding: 7bit";
    $header .= $crlf . "Content-Type: text/plain; charset=us-ascii";
  }
  if($attachment_msg || $send_format != 'PLAIN') {
    // TODO UNDO
    pontomail_sendmail_send_mime($to, $subject, $body, $header, $boundary, $send_format);
  } else {
    if(module_exist('html2txt')) {
      $plain_text = html2txt_convert($body, '75', TRUE);
      $body = $plain_text->text;
    }
    // TODO UNDO
    user_mail($to, $subject, $body, "From: $from\nReply-to: $from\nX-Mailer: Drupal\nReturn-path: $from\nErrors-to: $from");
  }
  
  
  // Send a copy if requested:
  if ($edit['copy']) {
    if($attachment || $send_format != 'PLAIN') {
      pontomail_sendmail_send_mime($from, $subject, $body, $header, $boundary, $send_format);
    } else {
      if(module_exist('html2txt')) {
        $plain_text = html2txt_convert($body, '75', TRUE);
        $body = $plain_text->text;
      }
      user_mail($from, $subject, $body, "From: $from\nReply-to: $from\nX-Mailer: Drupal\nReturn-path: $from\nErrors-to: $from");
    }
  }
  // Save if Neccesary
  $server_info = db_fetch_object(db_query("SELECT * FROM {pontomail_servers} WHERE sid = %d", $edit['sid']));
  if($server_info->save_remote_copy && $server_info->save_remote_copy_folder) {
    if(!$attachment || $send_format != 'PLAIN') {
      $header = "MIME-Version: 1.0\nContent-Type: multipart/mixed; boundary=\"$boundary\"\n" . $header;
    }
    $raw_msg = $header.$crlf.'To:'.$to.$crlf.'Subject:'.$subject.$crlf.$crlf.$body;
    $try = $imap_obj->msg_save($raw_msg, $imap_obj->base_con_str.$server_info->save_remote_copy_folder);
    if(!$try) {
      // TODO Better error msg
      form_set_error('', t('Error: Your msg could not be saved to the folder ' . 
        $imap_obj->get_folder_name($server_info->save_remote_copy_folder)));
    }
  }
  if($server_info->arvhice_sent_mail) {
    // Insert the raw msg into the database
    // TODO
  }
  // Log the operation:
  flood_register_event('pontomail_sendmail');
  watchdog('pontomail_sendmail', t('%name-from sent %name-to an e-mail.', array('%name-from' => theme('placeholder', $user->name), '%name-to' => theme('placeholder', $account->name))));

  // Set a status message:
  drupal_set_message(t('The message has been sent.'));

  // Jump to the user's profile page:
  drupal_goto("user/$account->uid");
}

function pontomail_sendmail_send_mime($mail, $subject, $message, $header, $boundary, $send_format = 'multipart/mixed') {
  if (variable_get('smtp_library', '') && file_exists(variable_get('smtp_library', ''))) {
   include_once './' . variable_get('smtp_library', '');
    return user_mail_wrapper($mail, $subject, $message, $header);
  }
  else {
    /*
    ** Note: if you are having problems with sending mail, or mails look wrong
    ** when they are received you may have to modify the str_replace to suit
    ** your systems.
    **  - \r\n will work under dos and windows.
    **  - \n will work for linux, unix and BSDs.
    **  - \r will work for macs.
    **
    ** According to RFC 2646, it's quite rude to not wrap your e-mails:
    **
    ** "The Text/Plain media type is the lowest common denominator of
    ** Internet email, with lines of no more than 997 characters (by
    ** convention usually no more than 80), and where the CRLF sequence
    ** represents a line break [MIME-IMT]."
    **
    ** CRLF === \r\n
    **
    ** http://www.rfc-editor.org/rfc/rfc2646.txt
    **
    */

    $header = "MIME-Version: 1.0\nContent-Type: multipart/mixed; boundary=\"$boundary\"\n" . $header;
    
    return mail(
      $mail,
      mime_header_encode($subject),
      str_replace("\r", '', $message),
      $header
    );
  }
}


/**
 * Autocomplet email address.
 */
function pontomail_sendmail_autocomplete($string) {
  // Get the tail end
  if($string == "") {
    return;
  }
  $items = split(',', $string);
  $search = trim($items[count($items) - 1]);
  unset($items[count($items) - 1]);
  $base_string = implode(',' ,$items);
  if($search == "") {
    return;
  }
  
  if(trim($base_string) != "") {
    $base_string .= ', ';
  }
  $matches = array();
  $result = db_query("SELECT name, mail FROM {users} WHERE name LIKE '%%%s%' OR mail like '%%%s%' ORDER BY name", $search, $search); 
  while($user = db_fetch_object($result)) {
    $matches[$base_string . $user->mail] = check_plain($user->name . " (" . $user->mail . ")");
  }
  # Check to see if we need to do LDAP Searching
  if(variable_get('pontomail_sendmail_ldap_enable', '')) {
    $server = variable_get('pontomail_sendmail_ldap_server', '');
    $port = variable_get('pontomail_sendmail_ldap_port', '');
    $type = variable_get('pontomail_sendmail_ldap_type', '');
    $user = variable_get('pontomail_sendmail_ldap_user', '');
    $pass = variable_get('pontomail_sendmail_ldap_pass', '');
    $dn = variable_get('pontomail_sendmail_ldap_dn', '');
    $filter = variable_get('pontomail_sendmail_ldap_filter', '');

    $ds = @ldap_connect($server); 
    
    if($type == 'ADS') {
      ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
      ldap_set_option($ds, LDAP_OPT_REFERRALS, 0);
    }

    $try = @ldap_bind($ds, $user, $pass);
    if($try) {
      if(trim($filter) != "") {
        $ldap_search = "(& (CN=*$search*) $filter)";
      } else {
        $ldap_search = "(CN=*$search*)";
      }
      $sr = ldap_search($ds, $dn, $ldap_search);
      $search_results = ldap_get_entries($ds, $sr);
      for($i=0; $i < $search_results['count']; $i++) {
        $name = $search_results[$i]['displayname'][0];
        $email = $search_results[$i]['mail'][0];
        if(valid_email_address($email)) {
          $matches[$base_string . $email] = check_plain("*" . $name . " (" . $email . ")");
        }
      }
    }
    @ldap_close($ds);
  }
  
  header("Cache-Control: no-cache, must-revalidate");
  header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
  print drupal_to_js($matches);
  exit();
}
