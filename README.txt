Pontomail Module
---------------------------------------------------------------------------------------------------------------------
Authors: Earnest Berry (aka Souvent22) earnest.berry [--at--] gmail [--dot--] com

Written as a small project to adhere to a larger intranet/enterprise solution.


ATRRIBUTIONS
------------------------------------------------------------------------------------------------

* pontomail_zip.inc
===================
This class is from Grant Hinkson (http://catalistcreative.com)
It is a derivitave of the Zend Article http://www.zend.com/zend/spotlight/creating-zip-files3.php

Class definition found at http://www.zend.com/zend/spotlight/creating-zip-files3.php
Some alterations to the original posted code were made in order to get everything working properly

Code was modifed again by Souvent22 to now use file names/paths, but take a raw data stream.



DESCRIPTION
---------------------------------------------------------------------------------------------------------------------
This module is a 'thin-client' for dupal to view email accounts. It works/looks simlar to Horde or any other
web mail client. Currently, only IMAP clients are supported until a solution to download and cache emails
internally is created.


DEPENDENCIES
---------------------------------------------------------------------------------------------------------------------
 - PHP5: utilizes PHP5's object oriented approach (PHP4 is untested, but may work)
 - Upload.module: Uses upload module when composing emails with attchments.
 - Mail Setup: That you have your mail settings in drupal setup.



INSTALLATION
---------------------------------------------------------------------------------------------------------------------

PRE-INSTALLATION INSTRUCTIONS:
------------------------------
i. Make sure upload.module is installed and working properly and enabled.
ii. Make sure that your email settings are correct and that Drupal can email items.


INSTALLATION INSTRUCTIONS:
------------------------------
1. Place the entire pontomail directory into your Drupal modules/ diretory.

2. Setup your email accounts: pontomail > settings

3. Your accounts will show up as menu items.

4. View/Read your emails.


NOTES
---------------------------------------------------------------------------------------------------------------------
* If you get an error about not being able to connect to your IMAP server; try messing around with the settings.
Sometimes setting 'No TLS' helps.

* Understand that 'secure' and 'ssl' are 2 differnt concepts. 

* Check your port numbers. Sometimes email servers use non-standard port numbers.
